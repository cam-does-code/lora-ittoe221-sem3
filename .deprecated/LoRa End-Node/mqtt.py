from umqttsimple import MQTTClient

# MQTT Configuration
mqtt_broker = "10.0.1.35"
mqtt_port = 1883
mqtt_topic = "decibel"
mqtt_client_id = "esp32_db"

mqtt_client = None

def connect_mqtt():
   global mqtt_client
   try:
      print(f"Connecting to MQTT broker at {mqtt_broker}:{mqtt_port}")
      mqtt_client = MQTTClient(mqtt_client_id, mqtt_broker, port=mqtt_port)
      mqtt_client.connect()
      print('Connected to %s MQTT broker' % (mqtt_broker))
   except Exception as e:
      print(f"Error connecting to MQTT: {e}")
      mqtt_client = None
   return mqtt_client

def mqtt_pub(value):
   global mqtt_client
   if mqtt_client is not None:
      try:
         mqtt_client.publish(mqtt_topic, str(value))
      except Exception as e:
         print(f"Error publishing to MQTT: {e}")
   else:
      print("MQTT client is not connected, cannot publish")

def mqtt_disconnect():
   global mqtt_client
   if mqtt_client is not None:
      try:
         mqtt_client.disconnect()
         print('Disconnected from MQTT broker')
      except Exception as e:
         print(f"Error disconnecting from MQTT: {e}")