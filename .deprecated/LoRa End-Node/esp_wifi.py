import network

def connect(SSID,Pass):
    wlan = network.WLAN(network.STA_IF) # create station interface
    wlan.active(True)       # activate the interface
    # wlan.scan()             # scan for access points
    if not wlan.isconnected():      # check if the station is connected to an AP
        print('connecting to network...')
        wlan.connect(SSID, Pass) # connect to an AP
    while not wlan.isconnected():
        pass
    print('network config:', wlan.ifconfig())
    # wlan.config('mac')      # get the interface's MAC address
    # wlan.ifconfig()         # get the interface's IP/netmask/gw/DNS addresses

def accesspoint(ap_ssid, ap_password):
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    ap.config(essid=ap_ssid, password=ap_password)
