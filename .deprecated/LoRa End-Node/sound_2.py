from machine import ADC, Pin
from time import sleep

SoundSensorPin = ADC(Pin(34))  # GPIO34 corresponds to A2 pin
VREF = 3.5  # Voltage on AREF pin, default: operating voltage

SoundSensorPin.atten(ADC.ATTN_11DB) # Kalibrering til korrekte målinger
SoundSensorPin.width(ADC.WIDTH_10BIT) # Fra 0 - 4095 range til 0 - 1023 range

def read_sound_level():
    voltage_value = SoundSensorPin.read_u16() / 65535.0 * VREF
    db_value = voltage_value * 50.0  # Convert voltage to decibel value
    print(voltage_value)
    
    return "{:.1f} dBA".format(db_value)

#while True:
    #read_sound_level()
    #sleep(0.2)