from machine import Pin, ADC, I2C
import ssd1306
import math
import time

# Initialize I2C for the OLED display
i2c = I2C(scl=Pin(22), sda=Pin(21))
oled = ssd1306.SSD1306_I2C(128, 64, i2c)

# Initialize analog sound sensor
sound_sensor = ADC(Pin(2))

def get_sound_dB(sensor_value):
    scaling_factor = 300  # Adjust as needed
    voltage = sensor_value * (5.0 / 4095.0) * scaling_factor

    # Check for low or zero voltage values
    if voltage <= 0:
        return float('-inf')  # Return negative infinity
    
    dB = 20 * math.log10(voltage)  # Convert voltage to decibels
    return dB

while True:
    sensor_value = sound_sensor.read()
    dB = get_sound_dB(sensor_value)
    
    oled.fill(0)
    oled.text("Sound Level", 0, 5)
    oled.text(str(round(dB, 2)) + " dB", 0, 25)
    oled.show()
    
    print("Sound Level:", round(dB, 2), "dB")
    print(sensor_value)

    time.sleep(0.5)
