import utime
import struct
import urandom
import network
from ulora import TTN, uLoRa
from machine import Pin

wlan = network.WLAN(network.STA_IF) # create station interface
wlan.active(True)       # activate the interface
# wlan.scan()             # scan for access points
if not wlan.isconnected():      # check if the station is connected to an AP
    print('connecting to network...')
    wlan.connect('CTnet', '12345678') # connect to an AP
    while not wlan.isconnected():
        pass
print('network config:', wlan.ifconfig())
# wlan.config('mac')      # get the interface's MAC address
# wlan.ifconfig()         # get the interface's IP/netmask/gw/DNS addresses

# Refer to device pinout / schematics diagrams for pin details
# (https://www.thethingsnetwork.org/forum/t/big-esp32-sx127x-topic-part-3/18436)
LORA_CS = 18
LORA_SCK = 5
LORA_MOSI = 27
LORA_MISO = 19
LORA_IRQ = 26
LORA_RST = 23
LORA_DATARATE = "SF7BW125"
LED = Pin(2, Pin.OUT)

# As shown on TTN's dashboard
REGION = "EU"
DEVICE_ADDRESS = bytearray([0x26, 0x0B, 0x00, 0xA0])
NETWORK_SESSION_KEY = bytearray([0xE9, 0x2E, 0xB8, 0xB7, 0xC6, 
                            0x36, 0x1F, 0x03, 0xBF, 0xDC, 0x09, 0x2A, 0x22, 0x0A, 0xCB, 0x8E])
APP_SESSION_KEY = bytearray([0x69, 0x75, 0xCA, 0x11, 0x68, 0x03,
                             0xF2, 0xDF, 0xCE, 0x3F, 0x03, 0x6A, 0x2B, 0xA3, 0xA7, 0xE7])

TTN_CONFIG = TTN(DEVICE_ADDRESS, NETWORK_SESSION_KEY, APP_SESSION_KEY, country=REGION)
FPORT = 1
lora = uLoRa(
    cs=LORA_CS,
    sck=LORA_SCK,
    mosi=LORA_MOSI,
    miso=LORA_MISO,
    irq=LORA_IRQ,
    rst=LORA_RST,
    ttn_config=TTN_CONFIG,
    datarate=LORA_DATARATE,
    fport=FPORT
)

while True:
    epoch = utime.time()
    temperature = urandom.randint(15, 30)
    payload = struct.pack('@Qh', int(epoch), int(temperature))
    lora.send_data(payload, len(payload), lora.frame_counter)
    LED.value(1)
    utime.sleep(1)
    LED.value(0)
    utime.sleep(5)
