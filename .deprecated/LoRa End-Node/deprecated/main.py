from SX1276_Tx import LoRa
from machine import Pin, I2C
import ssd1306, time, urandom
import esp_wifi, thingsnetwork, sound_2
from mqtt import connect_mqtt, mqtt_pub, mqtt_disconnect

# Initialize I2C for the OLED display
i2c = I2C(scl=Pin(22), sda=Pin(21))
oled = ssd1306.SSD1306_I2C(128, 64, i2c)

# Wifi configuration (connect and AP)
#esp_wifi.connect("Kirkevej 8", "61513041")
#esp_wifi.connect("Bennys iPhone 14 Pro", "61513041")
#esp_wifi.accesspoint("espAP","12345678")

# LoRa configuration TTGO LORA32
LoRa_MISO_Pin  = 19
LoRa_CS_Pin    = 18
LoRa_SCK_Pin   = 5
LoRa_MOSI_Pin  = 27
LoRa_G0_Pin    = 26  # DIO0_Pin
LoRa_EN_Pin    = 26
LoRa_RST_Pin   = 13
SPI_CH         = 1
LORA_DATARATE = "SF7BW125"
Pin(LoRa_EN_Pin, Pin.OUT).on()
lora = LoRa(LoRa_RST_Pin, LoRa_CS_Pin, SPI_CH, LoRa_SCK_Pin, LoRa_MOSI_Pin, LoRa_MISO_Pin, LoRa_G0_Pin)

lora.write('RegDioMapping1', lora.DioMapping['Dio0']['TxDone'])
lora.after_TxDone = lambda self: print('TxDone')

try:
    print("Attempting to connect to MQTT...")
    mqtt_client = connect_mqtt()
    if mqtt_client is not None:
        print("Connected to MQTT!")
        while True:
            sound_reading = sound_2.read_sound_level()
            #payload = str(urandom.randint(100, 999)) + ")Hello"
            payload = sound_reading
            payload_ttn = payload.encode('utf-8')
            print(payload)
            lora.send(payload)
            thingsnetwork.send(payload_ttn)
            mqtt_pub(payload)
            oled.fill(0)
            # oled.text("Sound Level", 0, 5)
            # oled.text(payload, 0, 25)
            # oled.show()
            time.sleep(5)
    else:
        print("MQTT connection failed, exiting...")
except KeyboardInterrupt:
    print(" Stopped!")
    mqtt_disconnect()