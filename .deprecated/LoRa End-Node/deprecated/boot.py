from SX1276_Tx import LoRa
from machine import Pin
import time, urandom
import sound_2, thingsnetwork

# LoRa configuration TTGO LORA32
LoRa_MISO_Pin  = 19
LoRa_CS_Pin    = 18
LoRa_SCK_Pin   = 5
LoRa_MOSI_Pin  = 27
LoRa_G0_Pin    = 26  # DIO0_Pin
LoRa_EN_Pin    = 26
LoRa_RST_Pin   = 13
SPI_CH         = 1
Pin(LoRa_EN_Pin, Pin.OUT).on()
lora = LoRa(LoRa_RST_Pin, LoRa_CS_Pin, SPI_CH, LoRa_SCK_Pin, LoRa_MOSI_Pin, LoRa_MISO_Pin, LoRa_G0_Pin)

lora.write('RegDioMapping1', lora.DioMapping['Dio0']['TxDone'])
lora.after_TxDone = lambda self: print('TxDone')

try:
    while True:
        sound_reading = sound_2.read_sound_level()
        payload = sound_reading
        payload_ttn = payload.encode('utf-8')
        print(payload)
        lora.send(payload)
        thingsnetwork.send(payload_ttn)
        time.sleep(5)
except KeyboardInterrupt:
    print(" Stopped!")