from ulora import uLoRa, TTN

# As shown on TTN's dashboard
REGION = "EU"
DEVICE_ADDRESS = bytearray([0x26, 0x0B, 0xFC, 0xCA])
NETWORK_SESSION_KEY = bytearray([0x94, 0xDA, 0x72, 0x9C, 0x07, 0x0E, 0x5F, 0x60, 0x04, 0xDE, 0xC4, 0xA2, 0xD6, 0x6E, 0x10, 0x78])
APP_SESSION_KEY = bytearray([0xF1, 0x9E, 0xA8, 0xBF, 0xD3, 0x2B, 0x4C, 0x9B, 0x45, 0x83, 0x67, 0xAA, 0x96, 0xC9, 0x25, 0xD2])

TTN_CONFIG = TTN(DEVICE_ADDRESS, NETWORK_SESSION_KEY, APP_SESSION_KEY, country=REGION)
FPORT = 1
lora = uLoRa(
    cs=18,
    sck=5,
    mosi=27,
    miso=19,
    irq=26,
    rst=13,
    ttn_config=TTN_CONFIG,
    datarate="SF9BW125",
    fport=FPORT
)

def send(payload):
    lora.send_data(payload, len(payload), lora.frame_counter)