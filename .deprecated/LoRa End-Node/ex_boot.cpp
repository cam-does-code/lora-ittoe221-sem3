#include <LoRa.h>
#include <Wire.h>
#include <ThingsNetwork.h>
#include <Sound_2.h>
#include <WiFi.h>
#include <MQTT.h>

const char* ssid = "YourWiFiSSID";
const char* password = "YourWiFiPassword";

const char* ttnAppEui = "YourTTNAppEUI";
const char* ttnAppKey = "YourTTNAppKey";
const char* ttnDevEui = "YourTTNDevEUI";

#define LoRa_MISO_Pin 19
#define LoRa_CS_Pin 18
#define LoRa_SCK_Pin 5
#define LoRa_MOSI_Pin 27
#define LoRa_G0_Pin 26
#define LoRa_EN_Pin 26
#define LoRa_RST_Pin 13
#define SPI_CH 1

int state = 0;

void setup() {
  pinMode(LoRa_EN_Pin, OUTPUT);
  digitalWrite(LoRa_EN_Pin, HIGH);
  Serial.begin(115200);

  if (!LoRa.begin(LoRa_CS_Pin, LoRa_SCK_Pin, LoRa_MOSI_Pin, LoRa_MISO_Pin, LoRa_RST_Pin, SPI_CH)) {
    Serial.println("LoRa initialization failed. Check your connections.");
    while (true);
  }

  LoRa.setDio0Action([]() {
    Serial.println("TxDone");
  });

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }

  Serial.println("Connected to WiFi");
  Serial.println("Connecting to TTN...");

  ttn.join(ttnAppEui, ttnDevEui, ttnAppKey);

  Serial.println("Connected to TTN");
}

void loop() {
  if (state == 0) {
    if (digitalRead(mqtt_pin) == LOW) {
      state = 1;
    }
    else if (digitalRead(mqtt_pin) == HIGH) {
      state = 2;
    }
  }

  if (state == 1) {
    int sound_reading = read_sound_level();
    String payload = String(sound_reading);
    Serial.println(payload);
    LoRa.beginPacket();
    LoRa.print(payload);
    LoRa.endPacket();
    thingsnetwork.sendBytes(payload.c_str(), payload.length());
    delay(5000);
    state = 0;
  }

  if (state == 2) {
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      delay(1000);
      Serial.println("Connecting to WiFi...");
    }

    Serial.println("Connected to WiFi");
    MQTTClient mqtt_client;
    connect_mqtt(mqtt_client);

    int sound_reading = read_sound_level();
    String payload = String(sound_reading);
    Serial.println(payload);
    LoRa.beginPacket();
    LoRa.print(payload);
    LoRa.endPacket();
    thingsnetwork.sendBytes(payload.c_str(), payload.length());
    mqtt_pub(mqtt_client, payload);
    delay(5000);
    state = 0;
  }
}
