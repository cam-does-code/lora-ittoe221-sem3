#include <vector>

#define SoundSensorPin 34  // This pin reads the analog voltage from the sound level meter
#define VREF 3.75          // Voltage on AREF pin, default: operating voltage

std::vector<float> soundMeasurements;

void soundSetup() {
    Serial.begin(115200);
}

void soundMeasure() {
  float voltage_analog, voltageValue, dbValue;
  voltage_analog = analogRead(SoundSensorPin);
  voltageValue = voltage_analog / 4096.0 * VREF;
  dbValue = voltageValue * 50.0;  // Convert voltage to decibel value
  
  // Store measurement in the vector
  soundMeasurements.push_back(dbValue);

  // Prepare payload for LoRaWAN
  String payload = String(dbValue, 1) + " dBA";

  // Print measurement
  Serial.println(payload);

  // Check if there is not a current TX/RX job running
  if (!(LMIC.opmode & OP_TXRXPEND)) {
    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, (uint8_t *)payload.c_str(), payload.length(), 0);
  }

  // Delay for stability
  delay(125); 
}

// Function to clear the vector of measurements
void clearMeasurementsList() {
    soundMeasurements.clear();
}