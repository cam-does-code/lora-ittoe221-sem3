#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SoundSensorPin 34  // This pin reads the analog voltage from the sound level meter
#define VREF 3.7           // Voltage on AREF pin, default: operating voltage

// OLED pins
#define OLED_SDA 21
#define OLED_SCL 22
#define OLED_RST 16
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

// Create an instance of the Adafruit_SSD1306 class
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);

  // Initialize OLED
  Wire.begin(OLED_SDA, OLED_SCL);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false, false)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  delay(2000);
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  // Display static text
  display.println("Hello, world!");
  display.display(); 
}

void loop() {
  float voltage_analog, voltageValue, dbValue;
  voltage_analog = analogRead(SoundSensorPin);
  voltageValue = voltage_analog / 4096.0 * VREF;
  dbValue = voltageValue * 50.0;  // Convert voltage to decibel value

  Serial.print(dbValue, 1);
  Serial.println(" dBA");
  Serial.println(voltage_analog);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.println("Sound Level");
  display.setCursor(0, 30);
  display.print(dbValue);
  display.setCursor(50, 30);
  display.print(" dBA");
  display.display();

  delay(250);
}
