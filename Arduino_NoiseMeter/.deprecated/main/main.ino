#include "loramac.h"
#include "boards.h"
#include <LoRa.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <WiFi.h>
#include <PubSubClient.h>

#define ServicePin 14  // Pin to enable/disable WiFi and MQTT for troubleshooting
#define SoundSensorPin 34  // This pin reads the analog voltage from the sound level meter
#define VREF 3.75           // Voltage on AREF pin, default: operating voltage

// OLED pins
#define OLED_SDA 21
#define OLED_SCL 22
#define OLED_RST 16
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

// Create an instance of the Adafruit_SSD1306 class
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Check if in Service Mode
bool ServiceEnabled = false;

// Wifi & MQTT Configuration
// const char* ssid = "Kirkevej 8";
// const char* ssid = "Bennys iPhone 14 Pro";
const char* ssid = "CTnet"; // Christian
// const char* password = "61513041";
const char* password = "12345678"; //Christian

// Add your MQTT Broker IP address, example:
// const char* mqtt_server = "10.0.1.40"; // home
// const char* mqtt_server = "172.20.10.2"; // iphone
const char* mqtt_server = "192.168.55.213"; // Christian

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

float dbValue;  // Declare dbValue as a global variable

void setup() {
    initBoard();
    // When the power is turned on, a delay is required.
    delay(1500);

    Serial.begin(115200);

    // setupLMIC();

    // Initialize LoRa communication
    LoRa.setPins(RADIO_CS_PIN, RADIO_RST_PIN, RADIO_DIO0_PIN);
    if (!LoRa.begin(LoRa_frequency)) {
        Serial.println("Starting LoRa failed!");
        while (1);
    }

    // Initialize OLED
    Wire.begin(OLED_SDA, OLED_SCL);
    if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false, false)) { // Address 0x3C for 128x32
        Serial.println(F("SSD1306 allocation failed"));
        for (;;); // Don't proceed, loop forever
    }
    delay(1000);
    display.clearDisplay();
    display.setTextSize(1.2);
    display.setTextColor(WHITE);
    display.setCursor(0, 0);
    display.println("Noise Meter Booting");
    display.display();
    delay(1000);

    // Initialize Wi-Fi only if the specified pin is HIGH
    if (digitalRead(ServicePin) == HIGH) {
        setup_wifi();
    }

    // Initialize MQTT only if the specified pin is HIGH
    if (digitalRead(ServicePin) == HIGH) {
        client.setServer(mqtt_server, 1883);
        client.setCallback(callback);
    }
}

void loop() {
    // Continuously monitor the input pin state
    bool enableState = digitalRead(ServicePin) == HIGH;

    // Check if the state has changed
    if (enableState != ServiceEnabled) {
        ServiceEnabled = enableState;

        // If WiFi and MQTT are now enabled, set them up
        if (ServiceEnabled) {
            setup_wifi();
            delay(1000);
            client.setServer(mqtt_server, 1883);
            client.setCallback(callback);
        }
        // If WiFi and MQTT are now disabled, disconnect them
        else {
            if (client.connected()) {
                client.disconnect();
            }
        }
    }

    // Execute WiFi and MQTT-related code only if enabled
    if (ServiceEnabled) {
        if (!client.connected()) {
            reconnect();
        }

        // Execute sound reading, display, LoRa sending, and MQTT publishing
        processSoundReading();
        processDisplayService();
        processLoRa();
        processMQTT();
        delay(250);

    } else {
        // If not in service mode, still execute sound reading, display, LoRa sending
        processSoundReading();
        processDisplay();
        processLoRa();
        //loopLMIC();
        delay(250);
    }
}

void processSoundReading() {
    // Sound Reading
    float voltage_analog, voltageValue;
    voltage_analog = analogRead(SoundSensorPin);
    voltageValue = voltage_analog / 4096.0 * VREF;
    dbValue = voltageValue * 50.0;  // Convert voltage to decibel value

    Serial.print(dbValue, 1);
    Serial.println(" dBA");
}

void processDisplay() {
    // Display on OLED
    display.clearDisplay();
    display.setCursor(0, 0);
    display.println("Normal Operation");
    display.setCursor(0, 25);
    display.println("Sound Level");    
    display.setCursor(0, 35);
    display.print(dbValue);
    display.setCursor(40, 35);
    display.print(" dBA");
    display.display();
}

void processDisplayService() {
    // Display on OLED
    display.clearDisplay();
    display.setCursor(0, 0);
    display.println("SERVICE MODE ACTIVE");
    display.setCursor(0, 25);
    display.println("Sound Level");    
    display.setCursor(0, 35);
    display.print(dbValue);
    display.setCursor(40, 35);
    display.print(" dBA");
    display.display();
}

void processLoRa() {
    // LoRa Send Packet
    LoRa.beginPacket();
    LoRa.print(dbValue); // Print the float value
    LoRa.print(" db");   // Concatenate the " db" string
    LoRa.endPacket();
}

void processMQTT() {
    // MQTT Publish
    String dbValueString = String(dbValue);
    client.publish("sound", dbValueString.c_str());
}

void setup_wifi() {
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    display.clearDisplay();
    display.setCursor(0, 0);
    display.println("SERVICE MODE");
    display.setCursor(0, 25);
    display.println("Wifi connected");
    display.setCursor(0, 50);
    display.print(WiFi.localIP());
    display.display();
}

void callback(char* topic, byte* message, unsigned int length) {
    Serial.print("Message arrived on topic: ");
    Serial.print(topic);
    Serial.print(". Message: ");
    String messageTemp;

    for (int i = 0; i < length; i++) {
        Serial.print((char)message[i]);
        messageTemp += (char)message[i];
    }
}

void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        String clientId = "ESP8266Client-" + String(random(0xffff), HEX);
        if (client.connect(clientId.c_str())) {
            Serial.println("connected");
            // Subscribe
            client.subscribe("esp32/output");
            display.clearDisplay();
            display.setCursor(0, 0);
            display.println("SERVICE MODE");
            display.setCursor(0, 25);
            display.println("MQTT connected");
            display.display();
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            display.clearDisplay();
            display.setCursor(0, 0);
            display.println("MQTT Failed");
            display.setCursor(0, 30);
            display.print("Trying again in 5 seconds");
            display.display();
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}
