// soundsensor.cpp
#include "soundsensor.h"
#include <Arduino.h>

std::vector<float> soundMeasurements;

void setupSoundSensor() {
    Serial.begin(115200);
    // Additional setup if needed
}

void soundMeasure() {
    float voltage_analog, voltageValue, dbValue;
    voltage_analog = analogRead(SoundSensorPin);
    voltageValue = voltage_analog / 4096.0 * VREF;
    dbValue = voltageValue * 50.0;  // Convert voltage to decibel value

    // Store measurement in the vector
    soundMeasurements.push_back(dbValue);

    // Limit the vector to a maximum of 14,400 readings
    if (soundMeasurements.size() > 14400) {
      soundMeasurements.erase(soundMeasurements.begin()); // Remove the oldest reading
    }

    // Print measurement
    Serial.print(dbValue, 1);
    Serial.println(" dBA");

    delay(250);
}

void clearMeasurementsList() {
    soundMeasurements.clear();
}
