#ifndef SQL_H
#define SQL_H

#include <sqlite3.h>

int openDb(const char *filename, sqlite3 **db);
int db_exec(sqlite3 *db, const char *sql);
int insertValues(const char *tableName, float value, unsigned long timestamp, sqlite3 *db);

#endif // SQL_H
