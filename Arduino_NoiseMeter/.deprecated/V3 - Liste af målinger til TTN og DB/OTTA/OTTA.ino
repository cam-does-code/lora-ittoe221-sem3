#include "loramac.h"
#include "boards.h"
#include "soundsensor.h"
#include "sql.h"

sqlite3* db;  // Declare a pointer to sqlite3

void setup()
{
    initBoard();
    // When the power is turned on, a delay is required.
    delay(1500);
    Serial.println("Noise Meter Boot");
    setupLMIC();
    setupSoundSensor();
    sqlite3_initialize();

    // Use forward slashes in the file path
    const char* dbPath = "C:/Users/benzd/Desktop/sound.db";
    Serial.print("Attempting to open database at: ");
    Serial.println(dbPath);

    if (sqlite3_open(dbPath, &db)) {
        Serial.println("Failed to open database.");
    }
    else {
      Serial.println("Database opened successfully.");
      // Create table if not exists
      db_exec(db, "CREATE TABLE IF NOT EXISTS measurements (id INTEGER PRIMARY KEY, value REAL, timestamp INTEGER)");
    }  
}

void loop()
{
    soundMeasure();
    loopLMIC();

    static unsigned long lastSaveMillis = 0;
    if (millis() - lastSaveMillis > 60000) { // Save every minute
      // Save values from soundMeasurements to the database
      for (float measurement : soundMeasurements) {
        insertValues("measurements", measurement, millis() / 1000, db);
      }

      lastSaveMillis = millis();
    }    
}
