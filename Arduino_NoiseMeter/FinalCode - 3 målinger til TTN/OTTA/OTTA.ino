#include "loramac.h"
#include "boards.h"
#include "soundsensor.h"

void setup()
{
    initBoard();
    // When the power is turned on, a delay is required.
    delay(1500);
    Serial.println("Noise Meter Boot");
    setupLMIC();
    setupSoundSensor();
}

void loop()
{
    loopLMIC();
    // Run sound measurement only if joined
    if (isJoined) {
        soundMeasure();
    }
}
