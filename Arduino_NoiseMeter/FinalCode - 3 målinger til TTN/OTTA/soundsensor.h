// soundsensor.h
#pragma once

#include <vector>

#define SoundSensorPin 34  // This pin reads the analog voltage from the sound level meter
#define VREF 3.75          // Voltage on AREF pin, default: operating voltage

extern std::vector<float> soundMeasurements;

void setupSoundSensor();
void soundMeasure();
void clearMeasurementsList();
