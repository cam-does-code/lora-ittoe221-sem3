## How I sent my first LoRaWAN message to The Things Network using a TTGO ESP32 & Micropython
https://medium.com/@JoooostB/how-i-send-my-first-lorawan-message-to-the-things-network-using-a-ttgo-esp32-micropython-a3fe447fff82

## The Things Network
https://www.thethingsnetwork.org/
https://eu1.cloud.thethings.network/console/applications/noise-project-ucl

## Lydmåler
https://www.dfrobot.com/product-1663.html


## Arduino biblioteker
https://github.com/Xinyuan-LilyGO/LilyGo-LoRa-Series/blob/master/docs/en/LilyGo_LoRa_Series_Quick_Start_On_TTN.md#lorawan-end-device
https://github.com/siara-cc/esp32_arduino_sqlite3_lib/tree/master


## Wifi og MQTT guide
https://randomnerdtutorials.com/esp32-mqtt-publish-subscribe-arduino-ide/